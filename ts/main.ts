// --------- BASIC TYPE ----------//
var message: string = "Hello World";
// console.log(message)
// --------- OBJECT TYPE ----------//
let singer = {
  name: "Duc Nguyen",
  info: "@ducnguyen256",
  class: "D13CNPM",
};
// console.log(singer.name);
// console.log(singer.info);
// console.log(singer.class);
//--------------- LOOPS ---------------------//
var n = 5;
while (n > 5) {
  console.log("Entered while");
}
do {
  console.log("Entered do…while");
} while (n > 5);
//--------------- Functions ---------------------//
/** truyền vào tham số */
function disp_details(id: number, name: string, mail_id?: string) {
  console.log("ID:", id);
  console.log("Name", name);

  if (mail_id != undefined) console.log("Email Id", mail_id);
}
//  disp_details(123,"John");
//  disp_details(111,"mary","mary@xyz.com");
/* truyền vào tham số không xác định */
function addNumbers(...nums: number[]) {
  var i;
  var sum: number = 0;

  for (i = 0; i < nums.length; i++) {
    sum = sum + nums[i];
  }
  console.log("sum of the numbers", sum);
}
//  addNumbers(1,2,3)
//  addNumbers(10,10,10,10,10)

/* truyèn vào tham số mặc định */
function calculate_discount(price: number, rate: number = 0.5) {
  var discount = price * rate;
  console.log("Discount Amount: ", discount);
}
//  calculate_discount(1000)
//  calculate_discount(1000,0.30)

/* hàm tạo hàm */
var myFunction = new Function("a", "b", "return a * b");
var x = myFunction(4, 3);
// console.log(x);

/* Đệ quy */
/* - tính giai thừa */
function factorial(number) {
  if (number <= 0) {
    // termination case
    return 1;
  } else {
    return number * factorial(number - 1); // function invokes itself
  }
}
console.log(factorial(5));

/*------------ Tuples --------------*/
var mytuple = [10, "Hello", "World", "typeScript"];
// // console.log("Items before push "+mytuple.length)    // returns the tuple size

// mytuple.push(12)                                    // append value to the tuple
// console.log("Items after push "+mytuple.length)
// console.log("Items before pop "+mytuple.length)
// console.log(mytuple.pop()+" popped from the tuple") // removes and returns the last item

// console.log("Items after pop "+mytuple.length)

/*--------------- Union ---------------*/
// Biến loại kết hợp
var val: string | number;
val = 12;
// console.log("numeric value of val "+val)
val = "This is a string";
// console.log("string value of val "+val)

//Loại Liên minh và tham số chức năng
function disp(name: string | string[]) {
  if (typeof name == "string") {
    console.log(name);
  } else {
    var i;

    for (i = 0; i < name.length; i++) {
      console.log(name[i]);
    }
  }
}
//  disp("mark")
//  console.log("Printing names array....")
//  disp(["Mark","Tom","Mary","John"])

// Kiểu Liên hợp và Mảng
var arr: number[] | string[];
var i: number;
arr = [1, 2, 4];
// console.log("**numeric array**");
for (i = 0; i < arr.length; i++) {
  //   console.log(arr[i]);
}
arr = ["Mumbai", "Pune", "Delhi"];
// console.log("**string array**");
for (i = 0; i < arr.length; i++) {
  //   console.log(arr[i]);
}

/*------------- Interfaces ----------------*/
// ~ Giao diện và Đối tượng (Interface and Objects)
interface IPerson {
  firstName: string;
  lastName: string;
  sayHi: () => string;
}

var customer: IPerson = {
  firstName: "Tom",
  lastName: "Hanks",
  sayHi: (): string => {
    return "Hi there";
  },
};

//  console.log("Customer Object ")
//  console.log(customer.firstName)
//  console.log(customer.lastName)
//  console.log(customer.sayHi())

var employee: IPerson = {
  firstName: "Jim",
  lastName: "Blakes",
  sayHi: (): string => {
    return "Hello!!!";
  },
};

//  console.log("Employee  Object ")
//  console.log(employee.firstName);
//  console.log(employee.lastName);

// ~ Loại Liên minh và Giao diện (Type and Interface)
interface RunOptions {
  program: string;
  commandline: string[] | string | (() => string);
}

//commandline as string
var options: RunOptions = { program: "test1", commandline: "Hello" };
//  console.log(options.commandline)

//commandline as a string array
options = { program: "test1", commandline: ["Hello", "World"] };
//  console.log(options.commandline[0]);
//  console.log(options.commandline[1]);

//commandline as a function expression
options = {
  program: "test1",
  commandline: () => {
    return "**Hello World**";
  },
};

var fn: any = options.commandline;
//  console.log(fn());

// ~ Giao diện và Mảng (Interfaces and Arrays)
// interface namelist {
//     [index:number]:string
//  }
//  var list2:namelist = ["John",1,"Bran"] //Error. 1 is not type string
//  interface ages {
//     [index:string]:number
//  }
//  var agelist:ages;
//  agelist["John"] = 15   // Ok
//  agelist[2] = 16 // Error

// ~ Kế thừa giao diện đơn giản(Simple Interface Inheritance)
interface Person {
  age: number;
}

interface Musician extends Person {
  instrument: string;
}

var drummer = <Musician>{};
drummer.age = 27;
drummer.instrument = "Drums";
//  console.log("Age:  "+drummer.age) console.log("Instrument:  "+drummer.instrument)

// ~ Kế thừa nhiều giao diện (Multiple Interface Inheritance)
interface IParent1 {
  v1: number;
}

interface IParent2 {
  v2: number;
}

interface Child extends IParent1, IParent2 {}
var Iobj: Child = { v1: 12, v2: 23 };
//  console.log("value 1: "+this.v1+" value 2: "+this.v2)

/*------------- Classes -----------------*/
class Car {
  //field
  engine: string;

  //constructor
  constructor(engine: string) {
    this.engine = engine;
  }

  //function
  disp(): void {
    console.log("Function displays Engine is  :   " + this.engine);
  }
}
//create an object
// var obj = new Car("XXSY1")
//access the field
// console.log("Reading attribute value Engine as :  "+obj.engine)
//access the function
// obj.disp()

// ~ Kế thừa lớp
class Shape1 {
  Area: number;

  constructor(a: number) {
    this.Area = a;
  }
}

class Circle1 extends Shape1 {
  disp(): void {
    console.log("Area of the circle:  " + this.Area);
  }
}

// var obj = new Circle(223);
// obj.disp()
//  ~ Kế thừa lớp và Ghi đè phương thức (Class inheritance and Method Overriding)
class PrinterClass {
  doPrint(): void {
    console.log("doPrint() from Parent called…");
  }
}

class StringPrinter extends PrinterClass {
  doPrint(): void {
    super.doPrint();
    console.log("doPrint() is printing a string…");
  }
}

// var obj = new StringPrinter()
// obj.doPrint()
// ~ Từ khóa tĩnh (The static Keyword)
class StaticMem {
  static num: number;
  static disp(): void {
    console.log("The value of num is" + StaticMem.num);
  }
}
// StaticMem.num = 12     // initialize the static variable
// StaticMem.disp()
// ~ Lớp học và Giao diện (Classes and Interfaces)
interface ILoan {
  interest: number;
}

class AgriLoan implements ILoan {
  interest: number;
  rebate: number;

  constructor(interest: number, rebate: number) {
    this.interest = interest;
    this.rebate = rebate;
  }
}

var obj1 = new AgriLoan(10, 1);
console.log("Interest is : " + obj1.interest + " Rebate is : " + obj1.rebate);

/*----------------- OPP ---------------------*/
// ~ Tính trừu tượng (Abstraction)
abstract class Employee {
  private name: string;
  private age: number;
  private qualification: string;

  constructor(name: string, age: number, qualification: string) {
    this.name = name;
    this.age = age;
    this.qualification = qualification;
  }
  abstract applyTheJob(): void; // phương thức abstract không chứa hàm thực thi
  abstract mailCheck(): void;
  getNameAge() {
    console.log(this.name + this.age);
  }
  getQualification() {
    console.log(this.qualification);
  }
}
//Kế thừa từ lớp trừu tượng: các class được kế thừa phải khai báo đầy đủ các phương thức abstract của class cha
class _employee extends Employee {
  constructor(name: string, age: number, qualification: string) {
    super(name, age, qualification);
  }
  applyTheJob() {
    console.log("Applying for web developer position");
  }
  mailCheck() {
    console.log("Confirm mail before the interview");
  }
}
// var ojb = new _employee('Duc', 22, 'Gà' );
// ojb.getNameAge();
// ojb.mailCheck();

// ~ Tính bao đóng (Encapsulation):
class Employee1 {
  private name: string;
  private age: number;
  private qualification: string;
  constructor(name: string, age: number, qualification: string) {
    this.name = name;
    this.age = age;
    this.qualification = qualification;
  }
  public getInfo() {
    console.log(`
            Name: ${this.name};
            age: ${this.age};
            Qualification: ${this.qualification}
            `);
  }
}
let _employee1 = new Employee1("NVA", 24, "bachelor");
// _employee1.getInfo();
/*Các thuộc tính trong class Employee1 để ở trạng thái private và chỉ có thể được lấy ra từ phương thức  public getInfo(), 
    như vậy đối tượng _employee1 sẽ được đóng gói. Từ đó class ScutiCompany có thể lấy được thông tin đối tượng employee từ phương 
    thức getInfo()*/
class ScutiCompany {
  public member: number;
  public name: string;
  constructor(name: string) {
    this.name = name;
  }
  static getInfoEmployee() {
    return _employee1.getInfo();
  }
}
// ScutiCompany.getInfoEmployee();

// ~ Tính kế thừa (Inheritance)
/* Có thể dễ dàng nhận thấy class ITcompany thừa kế từ class Company thông qua từ khóa extends, 
từ đó các phương thức và thuộc tính protected hay public đều được thế hiện ở class ITcompany  */
class Company {
  protected name: string;
  protected member: number;
  constructor(name: string, numberMember: number) {
    this.name = name;
    this.member = numberMember;
  }
  public getInformation() {
    console.log(`
            Welcome ${this.name} <br/>
            We have ${this.member} member
            `);
  }
}
class ITcompany extends Company {
  public makeWebsite(nameProject: string) {
    console.log("We are making" + nameProject);
  }
  public makeProduct(nameProject: string) {
    console.log(`We are developing ${nameProject} our own product `);
  }
}
let Scuti = new ITcompany("Scuti", 15);
// Scuti.getInformation();
// Scuti.makeWebsite('OutSourcing_projects');
// Scuti.makeProduct('CookStar');

// ~ Tính đa hình (Polymorphism)

/* Nhận thấy cùng là một phương thức tính chu vi và diện tích 
    được kế thừa từ class Shape nhưng với các đối tượng khác nhau, 
    thuộc tính khác nhau thì thực thi khác nhau */
const PI = 3.14;

class Shape {
  constructor(name: string) {}
  protected Perimeter() {}
  protected Acreage() {}
}

class Circle extends Shape {
  private radius: number;
  constructor(name: string, radius: number) {
    super(name);
    this.radius = radius;
  }
  Perimeter() {
    let perimeter = 2 * PI * this.radius;
    console.log("Perimeter is:" + perimeter);
  }
  Acreage() {
    let acreage = PI * this.radius * this.radius;
    console.log("Acreage is:" + acreage);
  }
}

class Square extends Shape {
  private length: number;
  private width: number;
  constructor(name: string, length: number, width: number) {
    super(name);
    this.length = length;
    this.width = width;
  }
  Perimeter() {
    let perimeter = (this.length + this.width) * 2;
    console.log("perimeter is :" + perimeter);
  }
  Acreage() {
    let acreage = this.length * this.width;
    console.log("acreage is :" + acreage);
  }
}
// let circle = new Circle('hinh tron',6);
// circle.Perimeter(); // Perimeter is:37.68
// circle.Acreage();   // Acreage is:113.03999999999999
// let square = new Square('hinh cn',8,9);
// square.Perimeter(); // Perimeter is :34
// square.Acreage();   // Acreage is :72

/*------------------------------------------------------------------------*/
