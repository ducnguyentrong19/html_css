var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// --------- BASIC TYPE ----------//
var message = "Hello World";
// console.log(message)
// --------- OBJECT TYPE ----------//
var singer = {
    name: "Duc Nguyen",
    info: "@ducnguyen256",
    "class": "D13CNPM"
};
// console.log(singer.name);
// console.log(singer.info);
// console.log(singer.class);
//--------------- LOOPS ---------------------//
var n = 5;
while (n > 5) {
    console.log("Entered while");
}
do {
    console.log("Entered do…while");
} while (n > 5);
//--------------- Functions ---------------------//
/** truyền vào tham số */
function disp_details(id, name, mail_id) {
    console.log("ID:", id);
    console.log("Name", name);
    if (mail_id != undefined)
        console.log("Email Id", mail_id);
}
//  disp_details(123,"John");
//  disp_details(111,"mary","mary@xyz.com");
/* truyền vào tham số không xác định */
function addNumbers() {
    var nums = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        nums[_i] = arguments[_i];
    }
    var i;
    var sum = 0;
    for (i = 0; i < nums.length; i++) {
        sum = sum + nums[i];
    }
    console.log("sum of the numbers", sum);
}
//  addNumbers(1,2,3)
//  addNumbers(10,10,10,10,10)
/* truyèn vào tham số mặc định */
function calculate_discount(price, rate) {
    if (rate === void 0) { rate = 0.5; }
    var discount = price * rate;
    console.log("Discount Amount: ", discount);
}
//  calculate_discount(1000)
//  calculate_discount(1000,0.30)
/* hàm tạo hàm */
var myFunction = new Function("a", "b", "return a * b");
var x = myFunction(4, 3);
// console.log(x);
/* Đệ quy */
/* - tính giai thừa */
function factorial(number) {
    if (number <= 0) {
        // termination case
        return 1;
    }
    else {
        return number * factorial(number - 1); // function invokes itself
    }
}
console.log(factorial(5));
/*------------ Tuples --------------*/
var mytuple = [10, "Hello", "World", "typeScript"];
// // console.log("Items before push "+mytuple.length)    // returns the tuple size
// mytuple.push(12)                                    // append value to the tuple
// console.log("Items after push "+mytuple.length)
// console.log("Items before pop "+mytuple.length)
// console.log(mytuple.pop()+" popped from the tuple") // removes and returns the last item
// console.log("Items after pop "+mytuple.length)
/*--------------- Union ---------------*/
// Biến loại kết hợp
var val;
val = 12;
// console.log("numeric value of val "+val)
val = "This is a string";
// console.log("string value of val "+val)
//Loại Liên minh và tham số chức năng
function disp(name) {
    if (typeof name == "string") {
        console.log(name);
    }
    else {
        var i;
        for (i = 0; i < name.length; i++) {
            console.log(name[i]);
        }
    }
}
//  disp("mark")
//  console.log("Printing names array....")
//  disp(["Mark","Tom","Mary","John"])
// Kiểu Liên hợp và Mảng
var arr;
var i;
arr = [1, 2, 4];
// console.log("**numeric array**");
for (i = 0; i < arr.length; i++) {
    //   console.log(arr[i]);
}
arr = ["Mumbai", "Pune", "Delhi"];
// console.log("**string array**");
for (i = 0; i < arr.length; i++) {
    //   console.log(arr[i]);
}
var customer = {
    firstName: "Tom",
    lastName: "Hanks",
    sayHi: function () { return "Hi there"; }
};
//  console.log("Customer Object ") 
//  console.log(customer.firstName) 
//  console.log(customer.lastName) 
//  console.log(customer.sayHi())  
var employee = {
    firstName: "Jim",
    lastName: "Blakes",
    sayHi: function () { return "Hello!!!"; }
};
//commandline as string 
var options = { program: "test1", commandline: "Hello" };
//  console.log(options.commandline)  
//commandline as a string array 
options = { program: "test1", commandline: ["Hello", "World"] };
//  console.log(options.commandline[0]); 
//  console.log(options.commandline[1]);  
//commandline as a function expression 
options = { program: "test1", commandline: function () { return "**Hello World**"; } };
var fn = options.commandline;
var drummer = {};
drummer.age = 27;
drummer.instrument = "Drums";
var Iobj = { v1: 12, v2: 23 };
//  console.log("value 1: "+this.v1+" value 2: "+this.v2)
/*------------- Classes -----------------*/
var Car = /** @class */ (function () {
    //constructor 
    function Car(engine) {
        this.engine = engine;
    }
    //function 
    Car.prototype.disp = function () {
        console.log("Function displays Engine is  :   " + this.engine);
    };
    return Car;
}());
//create an object 
// var obj = new Car("XXSY1")
//access the field 
// console.log("Reading attribute value Engine as :  "+obj.engine)  
//access the function
// obj.disp()
// ~ Kế thừa lớp
var Shape1 = /** @class */ (function () {
    function Shape1(a) {
        this.Area = a;
    }
    return Shape1;
}());
var Circle1 = /** @class */ (function (_super) {
    __extends(Circle1, _super);
    function Circle1() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Circle1.prototype.disp = function () {
        console.log("Area of the circle:  " + this.Area);
    };
    return Circle1;
}(Shape1));
// var obj = new Circle(223); 
// obj.disp()
//  ~ Kế thừa lớp và Ghi đè phương thức (Class inheritance and Method Overriding)
var PrinterClass = /** @class */ (function () {
    function PrinterClass() {
    }
    PrinterClass.prototype.doPrint = function () {
        console.log("doPrint() from Parent called…");
    };
    return PrinterClass;
}());
var StringPrinter = /** @class */ (function (_super) {
    __extends(StringPrinter, _super);
    function StringPrinter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    StringPrinter.prototype.doPrint = function () {
        _super.prototype.doPrint.call(this);
        console.log("doPrint() is printing a string…");
    };
    return StringPrinter;
}(PrinterClass));
// var obj = new StringPrinter() 
// obj.doPrint()
// ~ Từ khóa tĩnh (The static Keyword)
var StaticMem = /** @class */ (function () {
    function StaticMem() {
    }
    StaticMem.disp = function () {
        console.log("The value of num is" + StaticMem.num);
    };
    return StaticMem;
}());
var AgriLoan = /** @class */ (function () {
    function AgriLoan(interest, rebate) {
        this.interest = interest;
        this.rebate = rebate;
    }
    return AgriLoan;
}());
var obj1 = new AgriLoan(10, 1);
console.log("Interest is : " + obj1.interest + " Rebate is : " + obj1.rebate);
/*----------------- OPP ---------------------*/
// ~ Tính trừu tượng (Abstraction)
var Employee = /** @class */ (function () {
    function Employee(name, age, qualification) {
        this.name = name;
        this.age = age;
        this.qualification = qualification;
    }
    Employee.prototype.getNameAge = function () {
        console.log(this.name + this.age);
    };
    Employee.prototype.getQualification = function () {
        console.log(this.qualification);
    };
    return Employee;
}());
//Kế thừa từ lớp trừu tượng: các class được kế thừa phải khai báo đầy đủ các phương thức abstract của class cha 
var _employee = /** @class */ (function (_super) {
    __extends(_employee, _super);
    function _employee(name, age, qualification) {
        return _super.call(this, name, age, qualification) || this;
    }
    _employee.prototype.applyTheJob = function () {
        console.log('Applying for web developer position');
    };
    _employee.prototype.mailCheck = function () {
        console.log('Confirm mail before the interview');
    };
    return _employee;
}(Employee));
// var ojb = new _employee('Duc', 22, 'Gà' );
// ojb.getNameAge();
// ojb.mailCheck();
// ~ Tính bao đóng (Encapsulation):
var Employee1 = /** @class */ (function () {
    function Employee1(name, age, qualification) {
        this.name = name;
        this.age = age;
        this.qualification = qualification;
    }
    Employee1.prototype.getInfo = function () {
        console.log("\n            Name: ".concat(this.name, ";\n            age: ").concat(this.age, ";\n            Qualification: ").concat(this.qualification, "\n            "));
    };
    return Employee1;
}());
var _employee1 = new Employee1('NVA', 24, 'bachelor');
// _employee1.getInfo();
/*Các thuộc tính trong class Employee1 để ở trạng thái private và chỉ có thể được lấy ra từ phương thức  public getInfo(),
như vậy đối tượng _employee1 sẽ được đóng gói. Từ đó class ScutiCompany có thể lấy được thông tin đối tượng employee từ phương
thức getInfo()*/
var ScutiCompany = /** @class */ (function () {
    function ScutiCompany(name) {
        this.name = name;
    }
    ScutiCompany.getInfoEmployee = function () {
        return _employee1.getInfo();
    };
    return ScutiCompany;
}());
// ScutiCompany.getInfoEmployee();
// ~ Tính kế thừa (Inheritance)
/* Có thể dễ dàng nhận thấy class ITcompany thừa kế từ class Company thông qua từ khóa extends,
từ đó các phương thức và thuộc tính protected hay public đều được thế hiện ở class ITcompany  */
var Company = /** @class */ (function () {
    function Company(name, numberMember) {
        this.name = name;
        this.member = numberMember;
    }
    Company.prototype.getInformation = function () {
        console.log("\n            Welcome ".concat(this.name, " <br/>\n            We have ").concat(this.member, " member\n            "));
    };
    return Company;
}());
var ITcompany = /** @class */ (function (_super) {
    __extends(ITcompany, _super);
    function ITcompany() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ITcompany.prototype.makeWebsite = function (nameProject) {
        console.log('We are making' + nameProject);
    };
    ITcompany.prototype.makeProduct = function (nameProject) {
        console.log("We are developing ".concat(nameProject, " our own product "));
    };
    return ITcompany;
}(Company));
var Scuti = new ITcompany('Scuti', 15);
// Scuti.getInformation();
// Scuti.makeWebsite('OutSourcing_projects');
// Scuti.makeProduct('CookStar');
// ~ Tính đa hình (Polymorphism)
/* Nhận thấy cùng là một phương thức tính chu vi và diện tích
được kế thừa từ class Shape nhưng với các đối tượng khác nhau,
thuộc tính khác nhau thì thực thi khác nhau */
var PI = 3.14;
var Shape = /** @class */ (function () {
    function Shape(name) {
    }
    Shape.prototype.Perimeter = function () { };
    Shape.prototype.Acreage = function () { };
    return Shape;
}());
var Circle = /** @class */ (function (_super) {
    __extends(Circle, _super);
    function Circle(name, radius) {
        var _this = _super.call(this, name) || this;
        _this.radius = radius;
        return _this;
    }
    Circle.prototype.Perimeter = function () {
        var perimeter = 2 * PI * this.radius;
        console.log('Perimeter is:' + perimeter);
    };
    Circle.prototype.Acreage = function () {
        var acreage = PI * this.radius * this.radius;
        console.log('Acreage is:' + acreage);
    };
    return Circle;
}(Shape));
var Square = /** @class */ (function (_super) {
    __extends(Square, _super);
    function Square(name, length, width) {
        var _this = _super.call(this, name) || this;
        _this.length = length;
        _this.width = width;
        return _this;
    }
    Square.prototype.Perimeter = function () {
        var perimeter = (this.length + this.width) * 2;
        console.log('perimeter is :' + perimeter);
    };
    Square.prototype.Acreage = function () {
        var acreage = this.length * this.width;
        console.log('acreage is :' + acreage);
    };
    return Square;
}(Shape));
// let circle = new Circle('hinh tron',6);
// circle.Perimeter(); // Perimeter is:37.68
// circle.Acreage();   // Acreage is:113.03999999999999
// let square = new Square('hinh cn',8,9);
// square.Perimeter(); // Perimeter is :34
// square.Acreage();   // Acreage is :72
/*------------------------------------------------------------------------*/
