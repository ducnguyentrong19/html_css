- Biến:
    + const : được sử dụng để khai báo 1 hằng số, và giá trị của nó không thay đổi trong suốt chương trình.
    + let: khai báo biến chỉ có thể truy cập được trong block bao quanh nó được xác định bằng cặp {}.
    + var: khai báo biến có thể truy cập ở phạm vi hàm số hoặc bên ngoài hàm số, toàn cục.

- Kiểu dữ liệu:
    + Kiểu boolean
    + Kiểu null
    + Kiểu undefined
    + Kiểu số
    + Kiểu chuỗi
    + Kiểu Symbol (mới trong ECMAScript 6)
    + Kiểu đối tượng

- Object trong method (link: https://viblo.asia/p/object-methods-trong-javascript-moi-developers-can-phai-biet-bWrZnXxw5xw)
    + Object.create()
    + Object.keys()
    + Object.values()
    + Object.entries()
    + Object.assign()
    + Object.freeze()
    + Object.seal()

- Array method (link: https://anonystick.com/blog-developer/javascript-developer-javascript-nen-biet-nhung-method-arrays-nao-trong-javascript-x0ZB3R6E)
    *forEach()- Phương pháp này có thể giúp bạn lặp qua các item trên mảng.
    *includes()- xác định sự hiện diện của một phần tử trong một mảng.
    *filter()- Phương thức này nhằm tạo ra mộ mảng mới với các điều kiện mà các lập trình viên ai ai cũng biết.
    *map()- nó tạo ra một array mới bằng việc ta xử lý những item của array cũ bằng phương thức của chúng ta xử lý.
    *reduce()- thay thế cho map(), filter(), map() + filter(), some() or every(), [1,2,3,4] => [[1,2], [3,4]]
    *some()- Dạng như là kiểm tra xem có ít nhất thằng nào trong array vượt qua được cuộc kiểm tra không.
    *every()- Nó check hết item trong mảng thoả mãn điều kiện rồi trả về true or false.
    *sort()- Phương thức mặc định sort()chuyển đổi các loại phần tử thành chuỗi và sau đó sắp xếp chúng theo Thứ tự sắp xếp tăng dần
    *Array.from()- Cho phép bạn tạo các array từ một kiểu dữ liệu khác.
    *Array.of()- Có thể hiểu nôm na là khi truyền vào một số đơn, sẽ tạo giá trị đó như một element trong array, thay vì tạo ra số lượng các element đó.
    *Array.prototype.entries()
    *Array.prototype.copyWithin()
    find()-  sẽ trả về giá trị của phần tử thỏa mãn và bỏ qua không kiểm tra các phần tử còn lại.
    push()- Chèn một phần tử vào cuối mảng.
    unshift()- Chèn một phần tử vào đầu mảng.
    pop()- Xóa một phần tử ở cuối mảng.
    shift()- Bỏ một phần tử khỏi đầu mảng.
    slice()- Tạo một bản sao nông của một mảng.
    Array.isArray()- Xác định xem một giá trị có phải là một mảng hay không.
    length- Xác định kích thước của một mảng.
    concat()- kết nối 2 mảng
    join()- Phương thức này kết hợp tất cả các phần tử của mảng bằng dấu phân tách và trả về một chuỗi.
    fill()- phương thức thay đổi mảng ban đầu với giá trị tĩnh
    indexOf()- vị trí chỉ mục của một phần tử trong mảng
    reverse()- phương thức đảo ngược vị trí của các phần tử trong mảng

- Sync và ASync 
    + Sync Synchronous có nghĩa là xử lý đồng bộ, chương trình sẽ chạy theo từng bước và chỉ khi nào bước 1 thực hiện xong thì mới nhảy sang bước 2
    + ASync Asynchronous là xử lý bất động bộ, nghĩa là chương trình có thể nhảy đi bỏ qua một bước nào đó

- Promise 
    + là một cơ chế trong JavaScript giúp bạn thực thi các tác vụ bất đồng bộ mà không rơi vào callback hell hay pyramid of doom

- Biến ES6
    + var : 
        ~ được xác định có phạm vi toàn cục (global) hoặc phạm vi cục bộ/ hàm nếu khai báo trong hàm 
        ~ có thể khai báo lại;

    + let : 
        ~ được định phạm vi trong khối mã (Block-scoped) 
        ~ Một đoạn mã được bao bởi cặp dấu mở ngoặc nhọn và đóng ngoặc nhọn { }. Mọi lệnh trong cặp dấu là một khối mã (Block)
        ~ không được khai báo lại nhưng cập nhật được vd: let name = 'duc';
                                                            name = 'trong duc';
                                                            
    + const :  
        ~ được định phạm vi trong khối mã (Block-scoped) 
        ~ Một đoạn mã được bao bởi cặp dấu mở ngoặc nhọn và đóng ngoặc nhọn { }. Mọi lệnh trong cặp dấu là một khối mã (Block);
        ~ Không thể cập nhật và khai báo lại nghĩa là giá trị của biên không thể được thay đổi và không thể khai báo biến có trùng tên trong cùng phạm vi tồn tại

- function: context, bind
- arguments; default parameters:
    + arguments: trong JavaScript (dịch sang tiếng việt là các đối số) là một đối tượng có tính chất giống như mảng trong JavaScript, có khả năng truy cập được vào bên trong hàm và chứa các giá trị của các đối số đã được truyền cho hàm đó.
    + default parameters: cho phép các tham số được đặt tên được khởi tạo với các giá trị mặc định nếu không có giá trị nào hoặc undefined được truyền.

- call, apply, bind:
    + bind:
        ~ Trả ra hàm mới với 'this' tham chiếu 'thisArg'(Gọi hàm với this tham chiếu tới đối tượng khác)
        ~ Không thực hiện gọi hàm
        ~ Nếu được bind kèm 'arg1, arg2, ...' thì các đối số này sẽ được ưu tiên hơn sau gọi hàm bind đó
        vd: const newFn = fn.bind(thisArg, arg1,arg2, ....) => nếu truyền arg1,arg2(value) cho bind thì arg1,arg2(value) sẽ được uy tiên hơn
            newFn(arg1,arg2, ...) => muốn nhận arg1,arg2(value) ở hàm gọi, thì không thêm (value) ở hàm bind trên 
    + call:
        ~ Trả ra hàm mới với 'this' tham chiếu 'thisArg'(Gọi hàm với this tham chiếu tới đối tượng khác)
        ~ Thực hiện gọi hàm
        ~ Kế thừa thuộc tính, phương thức từ Constructor cha
        ~ Nhận các đối số cho hàm gốc từ arg1, arg2, ...
        vd: fn.call(thisArg, arg1,arg2, ....)
    + apply:
        ~ Trả ra hàm mới với 'this' tham chiếu 'thisArg'(Gọi hàm với this tham chiếu tới đối tượng khác)
        ~ Thực hiện gọi hàm
        ~ Kế thừa thuộc tính, phương thức từ Constructor cha
        ~ Nhận các đối số cho hàm gốc từ các đối số thứ 2 nhưng dưới dạng mảng
        vd: fn.call(thisArg, [arg1,arg2, ....])

- Enhanced object literals 
    + Định nghĩa key: value cho object
        vd: var name = 'javascript';
            var price = 1000;
            var course ={
                name,
                price
            } 
    + Định nghĩa method cho object
        vd: var name = 'javascript';
            var price = 1000;
            var course ={
                name,
                price,
                getName: function(){
                    return name;
                }
            } 
    + Định nghĩa key cho object dưới dạng biến
        vd: var fieldName = 'new-name';
            var fieldPrice = 'price';
            var course ={
                [fieldName]: 'javascript',
                [fieldPrice]: 1000,
            } 

- class, class inheritance, method overriding
    + class: ~ Sử dụng từ khóa classđể tạo một lớp học.
             ~ Luôn thêm một constructor()phương thức.
        vd: class ClassName {
                constructor() { ... }
                method_1() { ... }
                method_2() { ... }
                method_3() { ... }
            }
    + class inheritance: Một lớp được tạo bằng kế thừa lớp sẽ kế thừa tất cả các phương thức từ một lớp khác
    + method overriding: Ghi đè(Overriding) một phương thức
- super, static, rest, spea:
    + super: "Kế thừa" class cha, Gọi method từ class cha
    + static: Các phương thức của lớp tĩnh được định nghĩa trên chính lớp đó. Bạn không thể gọi một staticphương thức trên một đối tượng, chỉ trên một lớp đối tượng.
    + Spread: truyền tất cả các tham số được lấy ra từ mảng vào object.
        vd: let arr1 = [1, -2, 3, 4];
            let arr2 = [8, 3, -8, 1];
            console.log( Math.max(1, ...arr1, 2, ...arr2, 25) ); // 25
    + rest: truyền tham số đầu vào với số lượng không biết trước
        vd: function concat(sep, ...string){  //với string là tham số truyền vào 
                return string.join('sep');
            }
            concat('-', 'a', 'b', 'c', 'd');
- value type, reference type
    + value type : tham trị.
    + reference type : tham chiếu
- closure:
    + Closures có thể truy cập biến của hàm bên ngoài ngay cả hàm bên ngoài đã trả về
    + Closures lưu tham chiếu đến biến của hàm bên ngoài
- Higer order functions: “Hàm bậc cao hơn” là một hàm chấp nhận các hàm làm tham số và / hoặc trả về một hàm.
    
- Destructuring :  là một cú pháp cho phép bạn gán các thuộc tính của một Object hoặc một Array
    vd: const [a, b, c] = [1, 2, 3, 4, 5]
        console.log(a, b, c); // 1, 2, 3
    
        const [a, b, ...c] = [1, 2, 3, 4, 5]
        console.log(a, b, c) ; //1, 2, [3, 4, 5]


