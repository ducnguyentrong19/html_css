const a = 1; //const: được sử dụng để khai báo 1 hằng số, và giá trị của nó không thay đổi trong suốt chương trình.
let b; //let: khai báo biến chỉ có thể truy cập được trong block bao quanh nó được xác định bằng cặp {}.
var c; //var: khai báo biến có thể truy cập ở phạm vi hàm số hoặc bên ngoài hàm số, toàn cục.
const car = { type: "Fiat", model: "500", color: "white" };
// console.log(car);
var arr = [1, 2, 4, 5, 9, 6];
// console.log(arr);
/*------------------------------------------------------*/
//Tạo ra 1 hàm khởi tạo cơ sở (tựa như lớp cơ sở)
function Person(_age, _name) {
  this.age = _age;
  this.name = _name;
}
//Có thể thêm thuộc tính vào thuộc tính prototype của hàm khởi tạo
Person.prototype.showAge = function () {
  console.log(this.age);
};

//Tạo ra 1 hàm khởi tạo con (sẽ dùng để kế thừa lớp cơ sở)
function Student(_id, _score) {
  this.id = _id;
  this.score = _score;
}
//Thực hiện kế thừa, gán hàm khởi tạo của Animal cho prototype của Bird
Student.prototype = new Person(19, "Khoa");
Student.prototype.showScore = function () {
  console.log(this.score);
};

//Kiểm tra sự kế thừa
var student1 = new Student(01, 100);
student1.age = 19;
student1.showAge(); //19
student1.showScore(); //100
/*----------------------------------------*/
/*--------------------------------*/
// Initialize an object with properties and methods
const job = {
  position: "cashier",
  type: "hourlyyyyyyyyy",
  isAvailable: true,
  showDetails() {
    const accepting = this.isAvailable
      ? "is accepting applications"
      : "is not currently accepting applications";
    console.log(
      `The ${this.position} position is ${this.type} and ${accepting}.`
    );
  },
};

// Use Object.create to pass properties
const barista = Object.create(job);

barista.position = "=====";
// barista.showDetails();
/*--------------------------------*/
// if ... else
let width = window.innerWidth;
if (width > 750) {
  // console.log(width);
} else {
  // console.log('mobile');
}
/*--------------------------------*/
// map()
var arr = [10, 20, "hi", , {}];
arr.forEach(function (item, index) {
  // console.log(' arr['+index+'] is '+ item);
});

let itobj = new Map([
  ["x", 0],
  ["y", 1],
  ["z", 2],
]);

for (let kv of itobj) {
  //   console.log(kv);
}
// ['x', 0]
// ['y', 1]
// ['z', 2]

for (let [key, value] of itobj) {
  //   console.log(key,':',value);
}
/*--------------------------------*/
// Array method
const first = [1, 2, 3];
const second = [4, 5, 6];
const merged = first.concat(second);
// console.log(merged); // [1, 2, 3, 4, 5, 6]
// console.log(first); // [1, 2, 3]
// console.log(second); // [4, 5, 6]
const emotions = ["h", "e", "l", "l", "o"];
const joined = emotions.join("-");
// console.log(joined);

const names = ["tom", "alex", "bob", "john"];
names.indexOf("john"); // returns 1
// console.log(names.indexOf('john'));
names.indexOf("rob"); // returns -1
// JSON
let text =
  '{ "employees" : [' +
  '{ "firstName":"John" , "lastName":"Doe" },' +
  '{ "firstName":"Anna" , "lastName":"Smith" },' +
  '{ "firstName":"Peter" , "lastName":"Jones" } ]}';
const obj = JSON.parse(text);
// console.log(obj);

/*--------------------------------*/
// JavaScript để minh họa phương thức JSON.parse ().
var j = '{"Tên": "Krishna", "Email": "XYZ", "CN": "12345"}';
var data = JSON.parse(j);
// console.log('a', data);

// JavaScript để minh họa phương thức JSON.stringify ().
var j1 = { Tên: "Krishna", Email: "XYZ", CN: 12345 };
var data1 = JSON.stringify(j1);
// console.log('b', data1);

// Promise
var promise = new Promise(
  // Executor
  function (resolve, reject) {
    //Logic
    // Thành công : resolve
    // Thất bại : reject

    // fake call API;
    setTimeout(function () {
      resolve("I love You !!");
    }, 3000); // setTimeout
  }
);
promise
  .then(function (a) {
    // console.log(a);
  })
  .catch(function (b) {
    // console.log(b);
  })
  .finally(function () {
    // console.log('done!');
  });

// Async / Await
const getNewToDo = async (id) => {
  let response = await fetch(
    `https://jsonplaceholder.typicode.com/todos/${id}`
  );
  let data = await response.json();
  return data;
};
getNewToDo(2).then((data) => {
  // console.log('>>> check get data: ', data);
});

//   function: context, bind
const person = {
  firstName: "John",
  lastName: "Doe",
  fullName: function () {
    return this.firstName + " " + this.lastName;
  },
};

const member = {
  firstName: "hello",
  lastName: "wolrd",
};

let fullName = person.fullName.bind(member);
console.log(fullName());

//  Arrow function expression

const materials = ["Hydrogen", "Helium", "Lithium", "Beryllium"];

console.log(materials.map((material) => material.length));
// Template string
const x = 5;
const y = 10;
console.log(`Fifteen is ${x + y} and not ${2 * x + y}.`);
// arguments
function cong() {
  console.log(arguments[0] + arguments[1]);
}
cong(2, 18);
//  call, apply , bind
// +> bind
function log(level, time, message) {
  console.log(level + " - " + time + ": " + message);
}

// Không có this nên set this là null
// Set mặc định 2 tham số level và time
var logErrToday = log.bind(null, "Error", "Today");

// Hàm này tương ứng với log('Error', 'Today', 'Server die.')
logErrToday("Server die.");
//   +> call

// class inheritance
class Car {
  constructor(brand) {
    this.carname = brand;
  }
  present() {
    return "I have a " + this.carname;
  }
}

class Model extends Car {
  constructor(brand, mod) {
    super(brand); // phương thức trong phương thức khởi tạo, chúng ta gọi phương thức khởi tạo của cha mẹ và có quyền truy cập vào các thuộc tính và phương thức của cha.
    this.model = mod;
  }
  show() {
    return this.present() + ", it is a " + this.model;
  }
}

let myCar = new Model("Ford", "Mustang");
document.getElementById("demo2").innerHTML = myCar.show();
//   method overriding : Ghi đè(Overriding) một phương thức
class CoffeeMachine {
  makeCoffee() {
    console.log("pha cà phê");
  }
}
class SpecialCoffeeMachine extends CoffeeMachine {
  makeCoffee(callback) {
    console.log("Ưu tiên pha cà phê");
    callback();
  }
}
const coffeeMachine = new SpecialCoffeeMachine();
coffeeMachine.makeCoffee(function () {
  console.log("cho chủ tịch!");
});
